# docker-postgres-multi-schema

PostgreSQL with multiple schemas in Docker.

Based on https://hub.docker.com/_/postgres .

```
docker pull wrzlbrmft/postgres-multi-schema:<version>
```

This will create two schemas - `foo` and `bar`:

```
docker run -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_SCHEMAS="foo bar" wrzlbrmft/postgres-multi-schema:latest
```

Next, connect to `localhost:5432` with username `postgres` and password `postgres`.

See also:

  * https://www.postgresql.org/
  * https://hub.docker.com/r/wrzlbrmft/postgres-multi-schema/tags

## Kubernetes (GKE)

```
gcloud compute disks create postgres \
--size=10G \
--type=pd-ssd \
--zone=us-east4-a
```

```
helm install --set staticPersistentVolume=true postgres docker/helm/postgres-multi-schema
```

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
