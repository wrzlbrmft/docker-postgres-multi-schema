ARG TAG=alpine
FROM postgres:${TAG}

RUN apk upgrade --no-cache

COPY multi-schema.sh /docker-entrypoint-initdb.d/
