file_env POSTGRES_SCHEMAS
if [ -n "$POSTGRES_SCHEMAS" ]; then
    local schemaAlreadyExists
    for POSTGRES_SCHEMA in $POSTGRES_SCHEMAS; do
        schemaAlreadyExists="$(
            POSTGRES_SCHEMA= docker_process_sql --dbname "$POSTGRES_DB" --set schema="$POSTGRES_SCHEMA" --tuples-only <<-'EOSQL'
                SELECT 1 FROM information_schema.schemata WHERE schema_name = :'schema' ;
EOSQL
        )"
        if [ -z "$schemaAlreadyExists" ]; then
            POSTGRES_SCHEMA= docker_process_sql --dbname "$POSTGRES_DB" --set schema="$POSTGRES_SCHEMA" <<-'EOSQL'
                CREATE SCHEMA :"schema" ;
EOSQL
            echo
        fi
    done
fi
